#include <L/io.h>
#include <L/str.h>
#include <L/def.h>
#include <stdio.h>
using namespace L;

	namespace vm {
		enum opcode {
			hlt = 0,
			psh = 0x01,		je  = 0x08,
			add = 0x02,		jne = 0x09,
			sub = 0x03,		jg  = 0x0a,
			div = 0x04,		jge = 0x0b,
			mul = 0x05,		jl  = 0x0c,
			var = 0x06,		jle = 0x0d,
			cmp = 0x07,		jmp = 0x0e, // doubles as RET
		
			pop = 0x0f,
			dup = 0x10,
			tak = 0x11,
			ptr = 0x12,
			str = 0x13,
			msg = 0x14,
			dlg = 0x15,
			cal = 0x16,
			sto = 0x17, /* place in stack [STO val, loc] */
			stk = 0x18, /* get stack var (abs TAK)*/
			fpt = 0x20, /* flag push true */
			fpf = 0x21, /* flag push false */
			chc = 0x30,
			cnd = 0x31, /* test for equality, but only clear %sp-2 if eq */
			dbg = 0xff,
		};
		union si {
			s32 n;
			i16 ptr;
			const char* str;
		};
		struct state {
			si stack[32];
			const i8* program;
			struct flags {
				bool eq;
				bool lt;
				bool gt;
			} flags;
			i16 sp;
			i16 ip;
			i16 program_ln;
			state() {
				sp=0;
				ip=0;
			}
			s32 get32() {
				s32 i  = (program[ip++] << 24);
					i |= (program[ip++] << 16);
					i |= (program[ip++] << 8);
					i |= (program[ip++]);
				return i;
			}
			i16 get16() {
				i16 i  = (program[ip++] << 8);
					i |= (program[ip++]);
				return i;
			}
			void push(s32 s) {stack[sp++].n = s; }
			enum class stat {
				paused = 0,
				halted = 1,
				stack_overflow = 2,
				err = 5,
			};
			stat resume() {
				for (;;) if (sp==sz(stack)) return stat::stack_overflow; else switch (program[ip++]) {
				
					case hlt: return stat::halted;
					case psh: push(get32()); break;
					case add: {
						stack[sp-2].n += stack[sp-1].n; --sp;
						break;
					}
					case sub: {
						stack[sp-2].n -= stack[sp-1].n; --sp;
						break;
					}
					case div: {
						stack[sp-2].n /= stack[sp-1].n; --sp;
						break;
					}
					case mul: {
						stack[sp-2].n *= stack[sp-1].n; --sp;
						break;
					}
					case dbg: {
						if (sp==0)
							printf("stack empty\n");
						else for (i8 i = 0; i<sp; ++i){
							printf("stack[%d] = %d",i,stack[i].n);
							if ( (size_t)stack[i].str >= (size_t)&program[0] &&
								(size_t)stack[i].str <= (size_t)&program[program_ln]) { // trolololol
								printf(" (\"%s\")", stack[i].str);
							}
							printf("\n");
						}
						break;
					}
					case cmp: {
						flags.eq = (stack[sp-1].n == stack[sp-2].n);
						flags.lt = (stack[sp-1].n < stack[sp-2].n);
						flags.gt = (stack[sp-1].n > stack[sp-2].n);
						sp-=2;
						break;
					}
					case cnd: {
						flags.eq = (stack[sp-1].n == stack[sp-2].n);
						sp-=(flags.eq ? 2 : 1);
						break;
					}
					case jmp: { ip = stack[sp-1].ptr; --sp; break; }
					case je:  { if (flags.eq)				ip = stack[sp-1].ptr; --sp; break; }
					case jne: { if (not flags.eq)			ip = stack[sp-1].ptr; --sp; break; }
					case jl:  { if (flags.lt)				ip = stack[sp-1].ptr; --sp; break; }
					case jg:  { if (flags.gt)				ip = stack[sp-1].ptr; --sp; break; }
					case jle: { if (flags.lt or flags.eq)	ip = stack[sp-1].ptr; --sp; break; }
					case jge: { if (flags.gt or flags.eq)	ip = stack[sp-1].ptr; --sp; break; }
					case pop: { --sp; break; }
					case ptr: push(get16()); break;
					case str: { stack[sp++].str = (const char*)program+ip; while(program[ip++]!=0){}; break; }
					case msg: {
						i16 lines = str::len(stack[sp-1].str);
						const si* line = &stack[sp-(1+lines)];
						for(const char* fmt = stack[sp-1].str; *fmt!=0; ++fmt) {
							switch (*fmt) {
								case 1:
									printf("%s",line->str);
								break;
								case 2:
									printf("%d",line->n);
								break;
								default:
								
								break;
							}
							++line;
						}
						printf("\n");
						sp -= lines+1;
						break;
					}
					case sto: { stack[stack[sp-1].ptr] = stack[sp-2]; sp-=2; break; }
					case fpt: push(1); break;
					case fpf: push(0); break;
					case chc: {
						const i16 choices = stack[sp-1].ptr;
						for (i16 choice = choices; choice >0; --choice) {
							const i16 kind = stack[sp-(1+choice*2)].ptr;
							const char* str = stack[sp-(choice*2)].str;
							printf("* %d: %s\n", kind, str);
						}
						sp -= 1+choices*2;
						break;
					}
					case dlg: {
						s16 lines = stack[sp-1].n, fl = lines;
						i16 chr = stack[sp-(lines+2)].n;
						for(;lines!=0;--lines) {
							printf("CHAR %d: \"%s\"\n", chr, stack[sp-(lines+1)].str);
						}
						sp -= fl+1;
						break;
					}
					case tak: {
						stack[sp-1] = stack[sp-(1+stack[sp-1].ptr)];
						break;
					}
					case stk: {
						stack[sp-1] = stack[stack[sp-1].ptr];
						break;
					}
					case cal: {
						stack[sp].ptr = ip+2;
						ip = get16();
						sp++;break;
					}
					case dup: {
						stack[sp] = stack[sp-1];
						++sp;
					}
			}
			printf("finished program without halt!\n");
			return stat::err;
		}
	};
}

using namespace vm;
const i8 bin[] = {
	vm::ptr, 00, 00,
	vm::str, 'h','a','i',0,
	vm::ptr, 00, 01,
	vm::str, 'w','a','t',0,
	vm::ptr, 00,02,
	chc,dbg
};


int main() {
	vm::state s;
	s.program = bin;
	s.program_ln = sz(bin);
	return (int)s.resume();
}
