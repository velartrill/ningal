#include "win.h"
#include "controllers/title.h"
#include <L/io.h>
#include "data/marshal.h"
int main(int argc, char** argv) {
	win::init(&argc, argv);
	win::resize();
	/* queue the title screen */ {
		ui::ctr* c = new controllers::title;
		win::q(c,.5,0);
	}
	win::loop();
}