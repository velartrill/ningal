#pragma once
#include "../ui.h"
namespace ui {
	namespace layout {
		struct absbox : public view {
			px w, h;
			view* main;
			void render(px x, px y, px w, px h) const override;
			void frame() override;
			void handle(const event&, view* ctl) override;
		};
		struct split : public spacing {
			view* v1,* v2;
			float ratio;
			view* active;
		};
		struct hsplit : public split {
			void render(px x, px y, px w, px h) const override;
			void frame() override;
			void handle(const event&, view* ctl) override;
		};
		struct vsplit : public split {
			void render(px x, px y, px w, px h) const override;
		};
	}
}