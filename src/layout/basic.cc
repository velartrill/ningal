#include "basic.h"
#include <GLFW/glfw3.h>
namespace ui { namespace layout {
	void absbox::render(px x, px y, px _w, px _h) const {
		main -> render(x + (_w/2 - w/2),y + (_h/2 - h/2),w,h);
	}
	void absbox::frame() {
		main->frame();
	}
	
	void absbox::handle(const event& ev, view* ctl) {
		main->handle(ev,ctl);
	}
	void hsplit::render(px x, px y, px w, px h) const {
		x += margin; y += margin;
		w -= margin*2; h -= margin*2;
		v1->render(x, y, w*ratio - (margin + padding), h);
		v2->render(x+w*ratio, y, w*(1-ratio) - (margin+padding), h);
	}
	void hsplit::handle(const event& ev, view* ctl) {
		active -> handle(ev,ctl);
	}
	void hsplit::frame() {
		v1 -> frame();
		v2 -> frame();
	}
	void vsplit::render(px x, px y, px w, px h) const {
		
	}
}}