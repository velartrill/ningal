#pragma once
#include "../ui.h"
#include "../views/menu.h"
#include "../layout/basic.h"
namespace controllers {
	struct title : public ui::ctr {
		title();
		void run()          override,
			 render(px x, px y, px w, px h)
			          const override,
			 frame()         override;
		void handle(const ui::event&, view* ctl) override;
		static constexpr i8 particlec = 30;
		struct particle {
			px x, y;
			float dx, dy;
			float size;
			double life;
		} particles[particlec];
		/* views */
		ui::layout::absbox box;
		ui::menu menu;
			const static ui::menu::item items[];
	};
}