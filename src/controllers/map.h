#pragma once
#include "../ui.h"
#include "../data/map.h"
#include "../views/map.h"
namespace controllers {
	struct map : public ui::ctr	{
		map(const char* file);
		data::map cur;
		ui::map field;
		img* imgs;
		void run() override;
		void handle(const ui::event&, view* ctl) override;
		~map();
	};
	
}