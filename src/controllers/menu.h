#pragma once
#include "../ui.h"
#include "../layout/basic.h"
#include "../views/menu.h"
namespace controllers {
	struct menu : public ui::ctr {
		menu();
		void run() override;
		void handle(const ui::event&, view*) override;
		enum class menumode {inventory, talk} mode;
		ui::layout::hsplit panel;
			ui::menu commands;
			ui::menu::item command_items[5] = {
				{"Inventory",0},
				{"Talk",     0},
				{"Craft",    0},
				{"Save",     0},
				{"Quit",     0},
			};
			ui::menu chars;
			ui::menu::item char_items[4];
			ui::menu::entry* char_entries[4];
	};
}