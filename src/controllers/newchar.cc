#include "newchar.h"
#include "../win.h"
#include "../data/game.h"
#include "map.h"
namespace controllers {
	const ui::menu::item newchar::genders[] = {
		{"Female",0},
		{"Agender",0}
	};
	newchar::newchar() {
		gender.items = genders;
		gender.itemc = L::sz(genders);

		wizard.main = &gender;
		wizard.h = 90;
		wizard.w = 100;
		main = &wizard;
	}
	void newchar::handle(const ui::event& e, ui::view* ctl) {
		if (e.kind == ui::event::kind::handle) {
			data::game.player.name="";
			if (gender.cursor==0) {
				data::game.player.gender = data::character::gender::female;
			} else {
				data::game.player.gender = data::character::gender::agender;
			}
			win::q(&gender.openness,1,0, .3);
			data::game.player.name="Lexi";
			win::to(new map("trackless_wastes"));
		} else ctr::handle(e,this);
	}
	void newchar::run() {
		win::q(&gender.openness,0,1, .3);
	}
}