#pragma once
#include "../ui.h"
#include "../views/menu.h"
#include "../layout/basic.h"
namespace controllers {
	struct newchar : public ui::ctr {
		newchar();
		void run() override;
		void handle(const ui::event&, ui::view* ctl);
		ui::layout::absbox wizard;
			/* page 1 */
			ui::menu gender;
				const static ui::menu::item genders[];
	};
}