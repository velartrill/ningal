#include "menu.h"
#include "../win.h"
#include "../type.h"
#include "../data/game.h"
#include <GLFW/glfw3.h>
#include "inventory.h"
namespace controllers {
	menu::menu() {
		commands.items = command_items;
		commands.itemc = L::sz(command_items);
		commands.activator = &panel.active;
		chars.itemc = data::game.party.size + 1;
		char_items[0].name=data::game.player.name;
		char_items[0].flags=0;
		size_t i = 1;
		for (data::character& ch : data::game.party) {
			char_items[i].name=ch.name;
			char_items[i].flags=0;
			++i;
		}
		chars.items = char_items;
		chars.openness = 1;
		chars.activator = &panel.active;
		chars.size=30;
		panel.v1=&commands;
		panel.v2=&chars;
		panel.ratio=0.3;
		panel.active = &commands;
		panel.margin = 10;
		panel.padding = 0;
		main = &panel;
		commands.openness = 1;
	}
	void menu::run() {
	//	win::q(&commands.openness, 0, 1, .);
	}
	void menu::handle(const ui::event& e, view*) {
		if (e.kind==ui::event::kind::press) {
			if (e.key == GLFW_KEY_ESCAPE) {
				win::snd::blip();
				if (panel.active != &commands) {
					panel.active = &commands;
				} else win::qexit(.3);
			} else ctr::handle(e,this);
		} else if (e.kind==ui::event::kind::handle) {
			if (e.handle == &chars) {
				panel.active = &commands;
				switch (mode) {
					case menumode::inventory:
						if (chars.cursor==0)
							win::q(new inventory(&data::game.player));
						else
							win::q(new inventory(&data::game.party[chars.cursor-1]));
					break;
				}
			} else switch (commands.cursor) {
				case 0: mode=menumode::inventory;
						panel.active = &chars;
						break;
				case 4: win::qexit(0); win::qexit(0); //haaack
						break; 
			}
		} else ctr::handle(e, this);
	}
}