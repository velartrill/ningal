#pragma once
#include "../ui.h"
#include "../layout/basic.h"
#include "../views/menu.h"
#include "../data/char.h"
namespace controllers {
	struct inventory : public ui::ctr {
		inventory(data::character*);
		void handle(const ui::event& e, view* ctl) override;
		void run() override;
		ui::layout::absbox center;
		ui::menu list;
		ui::menu::imgitem list_items[5] = {
			{"icon/hands.png", "Hands", 0},
			{"icon/shirt.png", "Clothes", 0},
			{"icon/pants.png", "Pants", 0},
			{"icon/ring.png", "Accessory", 0},
			{"icon/backpack.png", "Back", 0},
		};
	};
}