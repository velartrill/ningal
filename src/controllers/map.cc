#include "map.h"
#include "../data/marshal.h"
#include "menu.h"
#include "../win.h"
#include <L/str.h>
#include <stdio.h>
#include <GLFW/glfw3.h>
namespace controllers {
	map::map(const char* file) {
		/* load the map from the right place */ {
			char path[16+L::str::len(file)];
			L::str::compose(path, "assets/map/", file);
			data::marshal::load::map(cur, path);
			for(i8 i = 0; i<cur.tilesetc; ++i) {
				auto& m = cur.tilesets[i];
				char path[14+L::str::len(m.name)];
				L::str::compose(path, "tileset/", m.name, ".png");
				m.img = new img(path);
			}

		}
		/* do shit */
		field.t = &cur;
		main = &field;
	}
	void map::handle(const ui::event& ev, view* ctl) {
		if (ev.kind==ui::event::kind::press and ev.key == GLFW_KEY_ESCAPE) {
			win::snd::blip();
			win::q(new menu, 0.3);
		} else ctr::handle(ev,this);
	}
	void map::run() {}
	map::~map() {
		for(i8 i = 0; i<cur.tilesetc; ++i) {
			delete cur.tilesets[i].img;
		}
		delete cur.tilesets;
		delete cur.layers;
	}
}
