#include "../win.h"
#include "title.h"
#include <GLFW/glfw3.h>
#include <L/io.h>
#include <L/rand.h>
#include <L/math.h>
#include "newchar.h"
#include <SOIL.h>
namespace controllers {
	namespace {
		void makeparticle(title::particle& p) {
			p.x = win::rand.range(0, win::width);
			p.y = win::rand.range(0, win::height);
			p.dx = ((float)(win::rand.range(0, 20)))/10.0f-1;
			p.dy = ((float)(win::rand.range(0, 20)))/10.0f-1;
			p.size = ((float)(win::rand.range(0, 10)))/10.0f;
			p.life = ((float)(win::rand.range(0, 10)))/10.0f;
		}
	}
	const ui::menu::item title::items[] = {
		{"New Game",0},
		{"Old Game",0},
		{"No Game",0},
	};
	title::title() {
		menu.items = items;
		menu.itemc = sizeof(items)/sizeof(ui::menu::item);
		box.main = &menu;
		box.w=400;
		box.h=130;
		main = &box;
		for (size_t i = 0; i < particlec; ++i) {
			makeparticle(particles[i]);
		}
	}
	void title::run() {
		win::q(&menu.openness,0,1,.3,0);
	}
	void title::render(px x, px y, px w, px h) const  {
		glColor4f(1,1,1,1);
		glEnable(GL_BLEND);
		for (size_t i = 0; i < particlec; ++i) {
			auto& p = particles[i];
			const float alpha = 1-(L::math::abs(p.life - .5)*2); //lol
			glColor4f(1,1,1,alpha);
			px size = 5 + p.size*10;
			glRecti(p.x,p.y,p.x+size,p.y+size);
		}
		glDisable(GL_BLEND);
		ctr::render(x,y,w,h);
	};
	void title::frame() {
		for (size_t i = 0; i < particlec; ++i) {
			auto& p = particles[i];
			auto e = win::elapsed();
			p.life -= e;
			if (p.life <= 0) {
				makeparticle(particles[i]);
			} else {
				p.x += p.dx*200.0f*e;
				p.y += p.dy*200.0f*e;
			}
		}
	};
	void title::handle(const ui::event& ev, view* ctl) {
		if (ev.kind == ui::event::kind::handle) {
			switch(menu.cursor) {
				case 0: 
					win::q(&menu.openness,1,0,.3,0);
					win::to(new newchar,.7);
				break;
				case 1:
				break;
				case 2: win::qexit(.5); break;
			}
		} else ctr::handle(ev, this);
	}
}