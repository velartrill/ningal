#include "inventory.h"
#include "../win.h"
#include <GLFW/glfw3.h>

namespace controllers {
	inventory::inventory(data::character* ch) {
		list.nav = &ui::menu::idx<ui::menu::imgitem>;
		
		list.openness = 1;
		list.items = list_items;
		list.itemc = 5;
		center.w = 400;
		center.h = 300;
		center.main = &list;
		main = &center;
	}
	void inventory::run() {

	}
	void inventory::handle(const ui::event& ev, view* ctl) {
		if (ev.kind==ui::event::kind::press and ev.key==GLFW_KEY_ESCAPE)
			win::qexit(.3);
		else ctr::handle(ev,this);
	}
}