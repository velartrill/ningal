#include "map.h"
#include "../ui.h"
#include "../data/map.h"
#include <GLFW/glfw3.h>
#include "../mgr.h"
#include <stdio.h>
#include "../win.h"
#include "../text.h"
#include <L/str.h>
namespace ui {
	map::map() {}
	void map::render(px x, px y, px w, px h) const {
		glEnable(GL_BLEND);
		for (i8 l = 0; l<t->layerc; l++) {
			L::str::charbuf buf;
			const px tlsw = 512/32, tlsh = 1024/32;
			const px size = 32;
			for (size_t iy = 0; iy<t->h; ++iy) {
			for (size_t ix = 0; ix<t->w; ++ix) {
				auto tile = t->layers[l].tiles[ix + (iy*t->w)];
				if (tile == 0) continue; // nothing there
				auto ts = fts(tile);
				px row = ts.idx / tlsw,
				   col = ts.idx % tlsw;
				glColor4f(1,1,1,1);
				ts.ts->img->slice(512,1024,ix*32, win::height - iy*size,size,size, 
					(col*size),(row*size),size,size);glColor3f(0,0,0);
				
			}
			}
			auto& ly = t->layers[l];
			//auto ts = fts()
		}
		glDisable(GL_BLEND);
	}

	map::fts_result map::fts(data::map::tile tl) const {
		fts_result res;
		for (i8 l = t->tilesetc-1;; --l) {
			if (tl>=t->tilesets[l].gid) {
				res.ts=&t->tilesets[l];
				res.idx=tl-t->tilesets[l].gid;
				return res;
			}
			if (l==0) mgr::die(3,"corrupt tileset!");
		}
	}
}