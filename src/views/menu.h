#pragma once
#include <GLFW/glfw3.h>
#include <L/def.h>
#include "../ui.h"
#include "../img.h"
namespace ui {
	struct menu : public view {
		struct entry {
			virtual void render(px x, px y, px w, px h,bool active,float alpha) const = 0;
		};
		struct item : public entry {
			const char* name;
			L::i8 flags;
			item() = default;
			item(const char* name, L::i8 flags);
			void render(px,px,px,px,bool,float) const override;
		};
		struct imgitem : public entry {
			const char* name;
			L::i8 flags;
			img img;
			imgitem() = default;
			imgitem(const char* img, const char* name, L::i8 flags);
			void render(px,px,px,px,bool,float) const override;
		};
		L::size_t itemc, cursor;
		const entry* items;
		double openness;
		view** activator = nullptr;
		template <typename T> static entry* idx(size_t i, const entry* ptr) {
			return (entry*) ((T*)ptr + i);
		};
		using nav_fn = entry* (*)(size_t, const entry* ptr);
		nav_fn nav = &idx<item>; // this is one of the worst things i've ever done tbh
		px size = 20;
		menu();
		void render(px x, px y, px w, px h) const override;
		void handle(const ui::event&, view* parent) override;
	};
}