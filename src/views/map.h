#pragma once
#include "../ui.h"
#include "../data/map.h"
namespace ui {
	struct map : public view {
		data::map* t;
		map();
		void render(px x, px y, px w, px h) const override;
		struct fts_result {
			data::map::tileset* ts;
			data::map::tile idx;
		};
		fts_result fts(data::map::tile t) const;
	};
}