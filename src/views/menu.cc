#include "menu.h"
#include <GLFW/glfw3.h>
#include "../text.h"
#include "../win.h"
#include <L/io.h>
namespace {
	template <i32 hex> void color(i8 alpha) {
		glColor4ub(
			((hex & 0xFF0000) >> 16),
			((hex & 0x00FF00) >> 8 ),
			(hex & 0x0000FF      ),
			alpha);
	}
	void body(px tx, px ty, px tw, px th) {
		glBegin(GL_LINE_STRIP);
			glVertex2f(tx,ty);
			glVertex2f(tx+tw-10,ty);
			glVertex2f(tx+tw,ty+10);
			glVertex2f(tx+tw,ty+th);
			glVertex2f(tx+10,ty+th);
			glVertex2f(tx,ty+th-10);
			glVertex2f(tx,ty);
		glEnd();
	}
}
namespace ui {
	menu::menu() {
		openness = 0;
		cursor = 0;
	}
	menu::item::item(const char* n, i8 f) {
		name=n;
		flags=f;
	}
	menu::imgitem::imgitem(const char* i, const char* n, i8 f) :
		img(i)
	{
		name=n;
		flags=f;
	}
	void menu::item::render(px x, px y, px w, px h, bool active, float alpha) const {
			if (active) glColor4f(1,1,1,alpha);
			else        glColor4f(1,1,1,.4*alpha);
			text(name,&font::sans,h,x,y,w,font::align::center);
	}
	void menu::imgitem::render(px x, px y, px w, px h, bool active, float alpha) const {
			if (active) glColor4f(1,1,1,alpha);
			else        glColor4f(1,1,1,.4*alpha);
			text(name,&font::sans,h,x+h+10,y,w-(h+10),font::align::left);

			img.render(x+5,y-2, h,h);
	}
	void menu::handle(const ui::event& ev, view* const ctl) {
		if (ev.kind==ui::event::kind::press) {
			switch (ev.key) {
				case GLFW_KEY_DOWN:
					if (cursor<itemc-1) ++cursor,
						win::snd::blip();
				break;
				case GLFW_KEY_UP:
					if (cursor!=0) --cursor,
						win::snd::blip();
				break;
				case GLFW_KEY_ENTER:
					ui::event e;
					e.kind=ui::event::kind::handle;
					e.handle=this;
					ctl->handle(e,ctl);
					win::snd::bleep();
				default:;
			}
		}
	}
	void menu::render(px x, px y, px w, px h) const {
		glEnable(GL_BLEND);
		const px tw = w * openness,
		         th = h * openness;
		const px tx = x + (w/2) * (1-openness),
		         ty = y + (h/2) * (1-openness);

		color<0x0D0709>(255*openness);
		glBegin(GL_POLYGON);
			body(tx,ty,tw,th);
		glEnd();
		color<0x451D2D>(255*openness);
		glBegin(GL_LINE_STRIP);
			body(tx,ty,tw,th);
		glEnd();
		const px gap = (size*2) * openness;
		const px iw = w - 20;
		glColor4f(1,1,1,openness);
		if (openness==1 and (activator == nullptr or *activator == this)) {
			const px l = x + 9,
			         b = win::height - (20+size+y+gap*cursor),
			         r = x + w - 10,
			         t = b + size+15;
			color<0x4A1F30>(255);
			glBegin(GL_POLYGON);
				glVertex2i(l+10,t);
				glVertex2i(r,t);
				glVertex2i(r,b+10);
				glVertex2i(r-10,b);
				glVertex2i(l,b);
				glVertex2i(l,t-10);
			glEnd();
			color<0x823655>(255);
			glBegin(GL_LINE_STRIP);
				glVertex2i(l+10,t);
				glVertex2i(r,t);
				glVertex2i(r,b+10);
				glVertex2i(r-10,b);
				glVertex2i(l,b);
				glVertex2i(l,t-10);
			glEnd();
		}
		for (size_t i = 0; i<itemc; ++i) {
			(*nav)(i, items)->render(x+10,win::height-(10+size+ty+gap*i),iw,size, cursor == i, openness);
		}
		glDisable(GL_BLEND);
	}
}