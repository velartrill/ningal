#include "win.h"
#include "mgr.h"
#include "text.h"
#include <L/list.h>
#include <L/stack.h>
#include <GLFW/glfw3.h>
#include <AL/al.h>
#include <AL/alut.h>
using namespace L;
using namespace L::types;
namespace libc { extern "C" {
	#include <time.h>
}}
namespace win {
	const i16 width = 1024, height = 640;
	const i8 fps = 60;
	GLFWwindow* w;
	ui::ctr* scene = nullptr;
	double time;
	namespace snd {
		ALuint source[2];
		ALuint bleep_buf, blip_buf;
		void load() {
			alGenSources(2, snd::source);
			bleep_buf = alutCreateBufferFromFile("assets/snd/bleep.wav");
			blip_buf = alutCreateBufferFromFile("assets/snd/blip.wav");
			alSourcei(source[0], AL_BUFFER, bleep_buf);
			alSourcei(source[1], AL_BUFFER, blip_buf);
		}
		void bleep() {
			alSourcePlay(source[0]);
		}
		void blip() {
			alSourcePlay(source[1]);
		}
	}
	void handle_key(GLFWwindow* win, int key, int scancode, int action, int mods) {
		ui::event ev;
		ev.kind = action ? ui::event::kind::press : ui::event::kind::release;
		ev.key = key;
		scene->handle(ev,scene);
	}
	void err(int code, const char* str) {
		mgr::ensure(0,str);
	} 
	void init(int* argc, char** argv) {
		glfwInit();
		glfwSetErrorCallback(err);
		glfwWindowHint(GLFW_SAMPLES, 8);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

		w = glfwCreateWindow(width,height,"DOOOOOM",nullptr,nullptr);
		mgr::ensure(w,"could not create window");
		glfwMakeContextCurrent(w);
		glDisable(GL_MULTISAMPLE);

		glfwSetKeyCallback(w,handle_key);

		int z = 0;
		alutInit(argc,argv);
		snd::load();

		rand.seed(libc::time(NULL));
	}
	void resize() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glViewport(0,0,width,height);
		glOrtho(0,width,0,height,-1,1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}

	struct act {
		double time;
		double elapsed;
		double delay;
		enum class kind_t {fn, tween, exit, enter, replace} kind;
		union {
			fn call;
			struct {double from; double to; double* twp;} tween;
			ui::ctr* to;
		};
	};
	list<act> timeline;
	stack<ui::ctr*>	sstack;
	act* curact = nullptr;
	L::rng <size_t> rand;
	double elapsed() {
		return glfwGetTime() - time;
	}
	void loop() {
		time = glfwGetTime();
		while(!glfwWindowShouldClose(w)) {
			glfwPollEvents();
			glClearColor(0,0,0,1);
			glClear(GL_COLOR_BUFFER_BIT);
			glColor3f(1,.5,.3);
			if (scene!=nullptr) scene->frame(), scene->render(0,0,width,height);
			begin_tl: if(curact != nullptr) { //is there something we need to be working on?
				curact -> elapsed += elapsed();
				if(curact->elapsed >= curact->time) do_curact:{ // *snicker*
					// do the thing
					switch(curact -> kind) {
						case act::kind_t::exit: {
							delete scene;
							if (sstack.top != nullptr) scene = --sstack;
							else return;
							break;
						}
						case act::kind_t::enter:
						case act::kind_t::replace: {
							scene->run();
							break;
						}
						case act::kind_t::fn: {
							(*curact->call)(scene);
							break;
						}
						case act::kind_t::tween: {
							*(curact->tween.twp)=curact->tween.to;
							break;
						}
					} 
					--timeline;
					curact = nullptr;
				} else {
					const double progress = 
						curact->elapsed > curact->time ? 1
						: (curact->elapsed / curact->time);
						
					switch(curact -> kind) {
						case act::kind_t::exit: {
							glColor4f(0,0,0,progress);
							goto fade;
							break;
						}
						case act::kind_t::enter: {
							glColor4f(0,0,0,1-progress);
							goto fade;
							break;
						}
						case act::kind_t::tween: {
							*(curact->tween.twp)=curact->tween.from +
							(curact->tween.to - curact->tween.from)*progress;
							break;
						}
						default:;
					}
					goto skip_fade;

					fade: { // draw overlay
						glEnable(GL_BLEND);
						glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
						glRecti(0,height,width,0);
						glDisable(GL_BLEND);
					}
					skip_fade:;
				}
			} else if(timeline.front != nullptr) { //nope, is there something in the timeline to move to?
				if (timeline.front -> val.delay>0) {
					timeline.front -> val.delay -= elapsed();
				} else {
					curact = &timeline.front->val;
					if (curact -> kind == act::kind_t::enter) {
						if (scene != nullptr) ++sstack = scene;
						scene = curact -> to;
					} else if (curact -> kind == act::kind_t::replace) {
						delete scene;
						scene = curact -> to;
					}
					if (curact->time == 0)	goto do_curact; // you'll pry my gotos from my cold,
					else 					goto begin_tl;  // dead fingers
				}
			}
			glfwSwapBuffers(w);
			time = glfwGetTime();
		}
	}
	
	void q(fn dst, float delay) {
		act& n = ++timeline;
		n.time = 0;
		n.delay = delay;
		n.kind = act::kind_t::fn;
		n.call = dst;
	}
	void q(ui::ctr* scene, float tm, float delay) {
		act& n = ++timeline;
		n.time = tm;
		n.delay = delay;
		n.kind = act::kind_t::enter;
		n.to = scene;
	}
	void to(ui::ctr* scene, float tm, float delay) {
		act& n = ++timeline;
		n.time = tm;
		n.delay = delay;
		n.kind = act::kind_t::replace;
		n.to = scene;
	}
	void q(double* d, double from, double to, float tm, float delay) {
		act& n = ++timeline;
		n.time = tm;
		n.delay = delay;
		n.kind = act::kind_t::tween;
		n.tween.twp = d;
		n.tween.from = from;
		n.tween.to = to;
	}
	void qexit(float tm, float delay) {
		act& n = ++timeline;
		n.time = tm;
		n.delay = delay;
		n.kind = act::kind_t::exit;
	}
}
