#include "type.h"
namespace vm {
	enum opcode {
		hlt = 0,
		psh = 0x01,		je  = 0x08,
		add = 0x02,		jne = 0x09,
		sub = 0x03		jg  = 0x0a,
		div = 0x04,		jge = 0x0b,
		mul = 0x05,		jl  = 0x0c,
		var = 0x06,		jle = 0x0d,
		cmp = 0x07,		jmp = 0x0e, // doubles as RET
		
		pop = 0x0f,
		dup = 0x10,
		tak = 0x11,
		ptr = 0x12,
		str = 0x13,
		msg = 0x14,
		dlg = 0x15,
		cal = 0x16,
		dbg = 0xff,
	};
	union si {
		s32 n;
		i16 ptr;
		const char* str;
	};
	struct state {
		si stack[16];
		const i8* program;
		struct flags {
			bool eq;
			bool lt;
			bool gt;
		} flags;
		i16 sp;
		i16 ip;
		state()
		s32 get32();
		i16 get16();
		enum class stat {
			paused,
			halted,
			stack_overflow
		};
		stat resume();
	};
}