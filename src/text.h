#pragma once
#include <FTGL/ftgl.h>
#include "type.h"
namespace font {
	extern FTGLPixmapFont sans, light, bold, mono;
	enum class align {left, right, center, justify};
}
void text(const char* t, FTFont* font, px size, px x, px y, px len, font::align a = font::align::left);
px getTextW(const char* t, FTFont* font, px size);
px getTextH(const char* t, FTFont* font, px size, px len);
void table(const char** t, const size_t* headers, px size, px x, px y, px w, px colw = 0, px padding = 5);
void table(const char** t, px size, px x, px y, px w, px colw = 0, px padding = 5);