#pragma once
#include <L/def.h>
#include "ui.h"
#include <L/rand.h>

namespace win {
	extern const L::i16 width, height;

	void init(int* argc, char** argv);     
	void resize();
	double elapsed();
	void loop();
	
	namespace snd {
		void blip();
		void bleep();
	}
	using fn = void (*)(ui::ctr*);
	using tween = void (*)(ui::ctr*, double);
	void q(fn dst, float delay = 0);
	void q(ui::ctr* scene, float tm = 1, float delay = 0);
	void to(ui::ctr* scene, float tm = 1, float delay = 0);
	void q(double* scene, double from, double to, float tm = 1, float delay = 0);
	void qexit(float tm = 1, float delay = 0);

	extern L::rng <size_t> rand;
}