#include "text.h"
#include <GL/gl.h>
namespace font {
	FTGLPixmapFont sans("assets/font/OpenSans-Regular.ttf");
	FTGLPixmapFont light("assets/font/OpenSans-Light.ttf");
	FTGLPixmapFont bold("assets/font/OpenSans-Bold.ttf");
	FTGLPixmapFont mono("assets/font/Inconsolata.otf");
}
static FTSimpleLayout layout;
void text(const char* t, FTFont* font, px size, px x, px y, px len, font::align a) {
	// this is a gross hack and I'm trying not to think about it too much
	// fwiw, I'm sorry
	layout.SetFont(font);
	layout.SetAlignment(
		a == font::align::center ? FTGL::ALIGN_CENTER :
		a == font::align::right ? FTGL::ALIGN_RIGHT :
		a == font::align::justify ? FTGL::ALIGN_JUSTIFY :
		FTGL::ALIGN_LEFT
	);
	font->FaceSize(size);
	layout.SetLineLength(len);
	glRasterPos2i(x,y);
	layout.Render(t);
}
px getTextW(const char* t, FTFont* font, px size) {
	font->FaceSize(size);
	layout.SetLineLength(0);
	return layout.BBox(t).Upper().X();
}
px getTextH(const char* t, FTFont* font, px size, px len) {
	font->FaceSize(size);
	layout.SetLineLength(len);
	return layout.BBox(t).Upper().Y();
}
// WARNING: headers should in most cases be null-terminated
void table(const char** t, const size_t* headers, 
			px size, px x, px y, px w, px colw, px padding) {
	if (colw==0) colw=w/2;
	size_t i = 0;
	while (t[i] != nullptr) {
		if (*headers == i) {
			++ headers;
			text(t[i++], &font::bold, size, x, y, w, font::align::center);
		} else {
			text(t[i++], &font::bold, size, x, y, colw - padding, font::align::right);
			text(t[i++], &font::sans, size, x+colw+padding, y, (w-colw) - padding, font::align::justify);
		}
		y-=size+padding;
	}
}
void table(const char** t, px size, px x, px y, px w, px colw, px padding) {
	if (colw==0) colw=w/2;
	while (*t != nullptr) {
		text(*t++, &font::bold, size, x, y, colw - padding, font::align::right);
		text(*t++, &font::sans, size, x+colw+padding, y, (w-colw) - padding, font::align::justify);
		y-=size+padding;
	}
}