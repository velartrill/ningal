#include "img.h"
#include <GLFW/glfw3.h>
#include <SOIL.h>
#include <L/str.h>
#include <stdio.h>
img::img(const char* name) {
	char path[8+L::str::len(name)];
	L::str::compose(path, "assets/", name);
	_id = SOIL_load_OGL_texture(path, SOIL_LOAD_AUTO,
	SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_POWER_OF_TWO);
}
img::~img() {
	glDeleteTextures(1,&_id);
}
void img::render(px x, px y, px w, px h) const {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _id);
	glBegin(GL_QUADS);
		glTexCoord2f(0,0); glVertex2f(x,y);
		glTexCoord2f(0,1); glVertex2f(x,y+h);
		glTexCoord2f(1,1); glVertex2f(x+w,y+h);
		glTexCoord2f(1,0); glVertex2f(x+w,y);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}
void img::slice(
	px bw, px bh,
	px x, px y, px w, px h,
	px xc, px yc, px wc, px hc
) const {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _id);
	const float z = float(wc) / float(bw), v = float(hc)/float(bh);
	const float top = 1.0f - (float(yc)/float(bh)),
				bottom = top - v,
				left = float(xc)/float(bw),
				right = left + z;
	glBegin(GL_QUADS);
		glTexCoord2f(left,bottom); glVertex2f(x,y);
		glTexCoord2f(left,top); glVertex2f(x,y+h);
		glTexCoord2f(right,top); glVertex2f(x+w,y+h);
		glTexCoord2f(right,bottom); glVertex2f(x+w,y);
	glEnd();
	glDisable(GL_TEXTURE_2D);	
}