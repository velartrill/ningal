#pragma once
#include "type.h"
namespace ui {
	struct event {
		enum class kind {press, release, handle} kind;
		union {
			int key;
			void* handle;
		};
	};
	struct view {
		virtual void render(px x, px y, px w, px h) const = 0;
		virtual void frame();
		virtual void handle(const event&, view* ctl);
	};
	struct group : public view {
		view** views;
		L::size_t viewc;
		view* active;
		virtual void render(px x, px y, px w, px h) const = 0;
		virtual ~group() = default;
		template <size_t N>
		void reg(view* (&dest)[N], view& w) {
			static_assert(N>=1, "array for reg too small");
			dest[0] = &w;
			viewc = 1;
			views = dest;
		}
		template <typename...Args, size_t N>
		void reg(view* (&dest)[N], view& w, Args... a) {
			static_assert(N>=sizeof...(Args), "array for reg too small");
			dest[sizeof...(Args)] = &w;
			reg(dest, a...);
			viewc=sizeof...(Args); //haaaack
		}
	};
	struct ctr : public view {
		view* main;
		virtual void run() = 0;
		virtual void render(px x, px y, px w, px h) const;
		virtual void handle(const event&, view* ctl) override;
		virtual ~ctr() = default;
	};
	struct spacing : public view {
		i16 margin = 0, padding = 0;
	};
}