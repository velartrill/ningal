#pragma once
#include "type.h"
struct img {
	i32 _id;
	img(const char* file);
	~img();
	void render(px x, px y, px w, px h) const;
	void slice(
		px bw, px bh, // size of base image
		px x, px y, px w, px h, // where to render
		px xc, px yc, px wc, px hc // section to cut out
	) const;
};
namespace imgcache {
	extern img
		backpack;
}