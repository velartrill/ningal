#include "marshal.h"
#include <L/file.h>
#include <L/str.h>
#include "../mgr.h"
namespace data {
	namespace marshal {
		struct stream {
			const unsigned char* data;
			size_t cursor = 0;
			stream(const char* file) {
				L::io::file fl(file);
				data = (const unsigned char*)fl.load();
			}
			i8 ri8() {
				return data[cursor++];
			}
			i16 ri16() {
				//LITTLE-ENDIAN ONLY
				return (ri8() << 8) | ri8();
			}
			char* str() {
				size_t l = L::str::len((const char*)data+cursor);
				char* s = new char[l+1];
				l = 0;
				while (data[cursor] != 0)
					s[l++] = data[cursor++];
				s[l]=0;
				++cursor;
				return s;
			}
		};
		namespace load {
			void map(struct map& map, const char* filename) {
				stream s(filename);
				const unsigned char sig[] = {s.ri8(), s.ri8(), s.ri8(), s.ri8()};
				if (not (sig[0] == 0x03 and sig[1] == 0xF9 and
					sig[2] == 0x8b and sig[3] == 0x03)) {
					mgr::die(2,"attempted to load invalid or corrupt map file!");
				}
				const i16 w = s.ri16(), h = s.ri16();
				map.w = w;
				map.h = h;
				map.name = s.str();
				map.tilesetc = s.ri8();
				//printf("%d %d %s %zu\n", map.w,map.h,map.name,map.tilesetc);
				map.tilesets = new map::tileset[map.tilesetc];
				for (int i = 0; i<map.tilesetc; ++i) {
					map.tilesets[i].name=s.str();
					map.tilesets[i].gid=s.ri16();
				}
				map.layerc = s.ri8();
				map::tile* space = new map::tile[w*h*map.layerc]; // captain, we are reading dangerously large
												// numbers in this sector!
				map.layers = new map::layer[map.layerc];
				for (int i = 0; i<map.layerc; ++i) {
					//printf("adding layer %d!\n",i);
					map.layers[i].tiles=space;
					space += w*h; // pppooOOIIIiinterrr mmmaaaAAAAtthhhh
					for (int t = 0; t<w*h; ++t) {
						map.layers[i].tiles[t] = s.ri16();
					}
				}
			}
		}
	}
}	