#include "../type.h"
#include <L/list.h>
namespace data {
	struct coin {
		const char* name;
		const char* icon;
	};
	struct money {
		coin* type;
		size_t quantity;
	};
	struct item {
		const char* name;
		const char* desc;
		enum type {
			none,
			backpack,
			weapon,
			clothes,
			pants,
			ring,
			amulet,
			flask,
			chalice,
			reagent
		} type;
		i8 wear;
	};

	struct weapon : public item {
		enum class use { melee, ranged } use;
		enum class size {tiny, small, med, big} size;
		i8 sharpness;
	};
	struct backpack : public item {
		i8 size;
		list<item> items;
	};
	struct chalice : public item {
		i8 size;
		i8 blood;
	};
	struct flask : public item {
		i8 size;
		i8 water;
	};
	struct clothes : public item {
		i8 pockets;
		i8 fanciness;
	};
	struct pants : public item {
		i8 pockets;
	};
}