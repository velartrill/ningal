#pragma once
#include "../type.h"

namespace data {
	typedef i8 blood_t;
	typedef i8 pain_t;
	enum class severity { none, light, severe, festering, lost };
	struct wound {
		severity injury, // blunt force trauma?
		         bleeding, // laceration
			     burns,
			     scars;
	};
	struct health_t {
		blood_t blood;
		wound head, chest, back, arms, legs, feet;
	};
	struct character {
		const char* name;
		enum gender {female, agender, male} gender;
		health_t health;
		pain_t pain();
		enum class life {awake, unconscious, comatose, dead} life;
	};
}