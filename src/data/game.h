#pragma once
#include "../type.h"
#include <L/list.h>
#include "map.h"
#include "char.h"
namespace data {
	struct world {
		character player;
		L::list<character> party;
		map* map;
		map::loc x, y;
	};
	extern world game;
}