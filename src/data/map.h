#pragma once
#include "../type.h"
#include "../img.h"
namespace data {
	struct map {
		typedef i16 loc;
		typedef i16 tile;
		struct tileset {
			const char* name;
			tile gid;
			img* img;
		};
		struct layer {
			tile* tiles;
		};
		const char* name;
		loc w, h;
		layer* layers; size_t layerc;
		tileset* tilesets; size_t tilesetc;
	};	
}