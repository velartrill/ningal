#pragma once
#include <L/def.h>
namespace mgr {
	void die(L::i8 ec, const char* err);
	void ensure(void* p, const char* err);
}