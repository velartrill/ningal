#include "mgr.h"
#include <L/io.h>
using namespace L;
using namespace L::types;
namespace mgr {
	namespace libc { extern "C" {
		#include "/usr/include/stdlib.h"
	}}
	void die(i8 ec, const char* err) {
		io::put(2,"\e[1;31m error: \e[0m");
		io::put(2,err);
		io::put(2,"\n\e[1m * exiting\e[0m\n");
		libc::exit(ec);
	}
	void ensure(void* p, const char* err) {
		if (p==nullptr) die(1, err);
	}
}
