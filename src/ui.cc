#include "ui.h"
namespace ui {
	void ctr::render(px x, px y, px w, px h) const {
		main->render(x,y,w,h);
	}
	void ctr::handle(const event& ev, view* ctl) {
		main->handle(ev,ctl);
	}
	void view::handle(const event& ev, view* ctl) {}
	void view::frame() {}
}