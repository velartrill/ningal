local file, dest = ...
if file == nil then
	print ("usage: lua convert.lua filename dest")
	return
end
map = dofile(file)

local outp = io.open(dest,"wb")


local j = { 0x03, 0xf9, 0x8b, 0x03 } -- signature

function i8(v)
	if (v>255) then
		error("Value too large!")
	else
		j[#j+1] = v
	end
end
function i16(v)
	if (v>65535) then
		error("Value too large!")
	else
		if (v<255) then j[#j+1] = 0
		else j[#j+1] = bit32.rshift(v,8) end
		j[#j+1] = v % 256
	end
end
function str(v)
	for i=1, v:len(), 1 do
		i8(v:byte(i))
	end
	i8(0)
end

i16 (map.width)
i16 (map.height)
str (map.properties.name)
i8  (#map.tilesets)
for i=1, #map.tilesets, 1 do
	str (map.tilesets[i].name)
	i16 (map.tilesets[i].firstgid)
end
i8  (#map.layers)
for i=1, #map.layers, 1 do
	for y=1, #map.layers[i].data, 1 do
		i16(map.layers[i].data[y])
	end
end

local f = string.char(unpack(j))
outp:write(f)
outp:close()
