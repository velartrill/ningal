exception CompilationError of string * string
exception BadChar of char * string
exception ExpectedClosing of string * string
exception ParseError of string

type 'a cell = {
	mutable v : 'a;
}

let rec times i (fn: int -> unit) : unit = if i <> 0 then fn i; times i-1;;

let dir_base : string cell = {v = ""}

let err x = print_string " \x1b[1;31m⟁ \x1b[0m"; print_string x; print_string "\n";;

let rec collapse (x:string list) : string = match x with
	| f::rs -> f ^"\n"^ (collapse rs)
	| [] -> ""
;;
let read filename =
	let list_lines filename = 
		let lines = ref [] in
		let chan = open_in filename in
		try while true; do
			lines := input_line chan :: !lines
		  done; []
		with End_of_file ->
		  close_in chan;
		  List.rev !lines in
	collapse(list_lines (dir_base.v^filename));;

type literal =
	| DStr of string
	| DInt of int
	| DAtom of string
	| DFlag of bool

type token =
	| RParen
	| LParen
	| RBracket
	| LBracket
	| TCol
	| TEnd
	| TEq
	| TLiteral of literal;;
let dump_literal (b: literal option) =
	match b with
		| None -> "\x1b[32m[unit]\x1b[0m"
		| Some (DInt x) -> "\x1b[34m"^(string_of_int x)^"\x1b[0m"
		| Some (DAtom x) -> "\x1b[35m"^x^"\x1b[0m"
		| Some (DStr x) -> "\x1b[33m\""^x^"\"\x1b[0m"
		| Some (DFlag true) -> "\x1b[32;1m✓\x1b[0m"
		| Some (DFlag false) -> "\x1b[31;1m✕\x1b[0m";;
		
let rec dump (x:token list) : unit = match x with
	| f::rs -> (match f with
		| RParen -> print_string "RPAREN,"
		| LParen -> print_string "LPAREN,"
		| TEnd -> print_string "TEND,"
		| (TLiteral (DAtom a)) -> Printf.printf "ATOM(%s)," a
		| TLiteral s -> Printf.printf "LITERAL(%s)," (dump_literal (Some s))
		| TEq -> print_string "TEq,"
		| RBracket -> print_string "RBracket,"
		| LBracket -> print_string "LBracket,"
		| TCol -> print_string "TCol,"
	); dump rs
	| [] -> ()

type lex_state = {
	mutable p         : int;
	mutable buf       : string;
	mutable tokens    : token list;
	mutable cp        : token_kind;
} and token_kind =
	PNull | PStr | PInt | PAtom;;
	
let lex (tx:string) (filename:string) = 
	let st = {
		p         = 0;
		buf       = "";
		tokens    = [];
		cp        = PNull
	} in
	let push (t:token) : unit =
		st.tokens <- st.tokens @ [t] in
	let buf (c:char) : unit  =
		st.buf <- st.buf ^ String.make 1 c in
	let register () = push (match st.cp with
		| PNull -> assert false
		| PStr -> TLiteral(DStr st.buf)
		| PInt -> TLiteral(DInt (int_of_string st.buf))
		| PAtom -> match st.buf with
			| "off" -> TLiteral(DFlag false)
			| "on" -> TLiteral(DFlag true)
			| _ -> TLiteral(DAtom st.buf)
	); st.buf <- ""; st.p <- st.p - 1; (*haaack*) st.cp <- PNull  in
	while st.p < String.length tx; do let c = tx.[st.p] in 
		(match st.cp with
			| PNull -> (match c with
						| ' ' | '\n' | '\t'   -> ()
						| '0'..'9'            -> buf c; st.cp <- PInt
						| 'a'..'z' | 'A'..'Z' -> buf c; st.cp <- PAtom
						| '{'|'(' -> push RParen
						| '}'|')' -> push LParen
						| ';' -> push TEnd
						| ':' -> push TCol
						| '=' -> push TEq
						| '\'' -> st.cp <- PStr
						| _ -> raise (BadChar (c, filename)))
			| PStr -> (match c with
						| '\'' -> st.p <- st.p + 1; register()
						| '\\' -> st.p <- st.p + 1; buf tx.[st.p]
						| _ -> buf c)
			| PInt -> (match c with
						| '0'..'9' -> buf c
						| _ -> register())
			| PAtom -> (match c with
						| 'a'..'z' | 'A'..'Z' | '-' | '_' | '0'..'9' -> buf c
						| _ -> register()));
		st.p <- st.p + 1;
	done;
	(match st.cp with
		| PStr -> raise (ExpectedClosing ("quote (')", filename))
		| PNull -> ()
		| _ -> register());
	st.tokens;;


type node =
	| Dialogue of string * string
	| Directive of string * literal option
	| Value of literal
	| Definition of string * literal
	| Assignment of string * literal
	| Box of literal * literal option * node list;;


let rec parse (t:token list) : (node list) * (token list) = match t with
	| ((TLiteral (DAtom a)))::TCol::TLiteral(DStr(l))::TEnd::rs -> let (s,n) = parse(rs) in
		((Dialogue(a,l)::s),n)
	| ((TLiteral (DAtom a)))::TEnd::rs				        -> let (s,n) = parse(rs) in
		((Directive(a,None)::s),n)
	| ((TLiteral (DAtom a)))::(TLiteral b)::TEnd::rs		    -> let (s,n) = parse(rs) in
		((Directive(a,Some b)::s),n)


	| (TLiteral (DAtom "let"))::(TLiteral (DAtom vn))::TEq::(TLiteral vl)::TEnd::rs		-> let (s,n) = parse(rs) in
		((Definition(vn,vl)::s),n)
	| (TLiteral (DAtom "set"))::(TLiteral (DAtom vn))::TEq::(TLiteral vl)::TEnd::rs		-> let (s,n) = parse(rs) in
		((Assignment(vn,vl)::s),n)

	| (TLiteral a)::TEnd::rs				-> let (s,n) = parse(rs) in
		(((Value a)::s),n)

	| (TLiteral a)::RParen::rs	->
		let (s,n) = parse(rs) in
		let (ss,nn) = parse(n) in
		(Box(a,None,s)::ss), nn
					
	| (TLiteral a)::(TLiteral p)::RParen::rs ->
		let (s,n) = parse(rs) in
		let (ss,nn) = parse(n) in
		(Box(a,Some p,s)::ss), nn

	| LParen::rs -> [], rs
	| [] -> [], []
	| TEnd::rs -> let (i,_) = parse(rs) in i, []
	| _ -> raise (ParseError "") (* haaack *)
and qparse (filename:string) = try
	let lexed = lex (read filename) filename in
	let (i,_) = parse lexed in i
with ParseError(_) -> raise (ParseError filename);;
	

let dump_ast (ast:node list) : unit =
	let rec space (i:int) : string =
		if i> 0 then ("   " ^ space(i-1)) else ""
	in let rec loop top (i:int) : unit =
		match top with
			| [] -> ()
			| x::rs ->
			(match x with
				| Directive (a, b) -> print_string (space i^"▻ "^a^" "^(dump_literal b)^"\n")
				| Box (a,b,c) ->
					print_string (space i^"▾ \x1b[1m"^(dump_literal (Some a))^"\x1b[0m "^(dump_literal b)^"\n");
					loop c (i+1);
				| Definition (a,b) -> print_string (space i ^"▻ \x1b[1m"^a^"\x1b[0m = "^(dump_literal (Some b))^"\n")
				| Assignment (a,b) -> print_string (space i ^"▻ \x1b[1m"^a^"\x1b[0m ← "^(dump_literal (Some b))^"\n")
				| Dialogue(name,line) -> print_string (space i^"☺ \x1b[1m"^name^"\x1b[0m says \x1b[36m\""^line^"\"\x1b[0m\n")
				| Value(a) -> print_string (space i^"* "^(dump_literal (Some a))^"\n")
			); loop rs i
	in loop ast 0;;


type character = {
	name : string;
	id : string;
	face : string;
	spriteset : string;
}

let rec bitstr (x:int list) : string = match x with
	| i::rs -> (String.make 1 (Char.chr(i))) ^ bitstr rs
	| [] -> ""

let bitstr_of_string (str:bytes) : int list =
	let rec extract (i:int) : int list = (
		if (i = String.length str) then [0]
		else ((int_of_char str.[i]) :: (extract (i+1)))
	) in
	extract 0;;

let rec each l fn = match l with
	| h::rs -> fn h; each rs fn
	| [] -> ();;

let rec map fn l = match l with
	| h::rs -> (fn h)::map fn rs
	| [] -> [];

type behavior = {
	id : string;
	img : string;
	name : string;
	flag : string option;
}

type manifest_data = {
	title : string;
	bhv : behavior list;
	src_char : string list; (* filename *)
	src_script : string list; (* filename *)
}

module Scripting = struct
	type routine = {
		id      : string;
		dat     : node list;
		filename: string; 
		(* (Scripting.compile filename id dat) *)
	}
	type defer = {
		addr : int;
		id : string;
		skip : bool;
	}
	type section = {
		id : string;
		start : int;
		stop : int;
	}
	type ltype = LStr
	           | LInt
	           | LPtr
	           | LFlag
   	let int_of_type x = match x with
			| LStr -> 1
			| LInt -> 2
			| LFlag -> 3
			| LPtr -> 4
	let str_of_type x = match x with
			| LStr -> "string"
			| LInt -> "int"
			| LFlag -> "flag"
			| LPtr -> "pointer";;
	type var = {id: string; ty: ltype; pos: int; (* stack position *) }
	type cpstate = {
		mutable addr : int;
		mutable stackp : int;
		mutable defers : defer list;
		mutable sections : section list;
		mutable code : int list;
		mutable vars : var list;
	}
	type choice = {
		bhv : int;
		text : string;
		nodes : node list;
	}
	let processFile (filename:string) : routine list =
		let ast = qparse(filename) in
		let rec loop (i:node list) : routine list = match i with
			| Box(DAtom("script"),Some(DAtom id),dat)::rs ->
				{id = id; dat=dat; filename=filename}::loop rs
			| Box(DAtom("state"),None,dat)::rs ->
				raise (CompilationError("compilation-unit locals not yet supported", filename))
			| [] -> []
			| _ -> (raise (CompilationError("invalid entry in script file", filename)))
		in loop ast;;
	let compile (r:routine) (routinelist:routine list) (manifest:manifest_data) : bytes =
		let state : cpstate = {
			addr = 0;
			stackp = 0;
			defers = [];
			code = [];
			sections = [];
			vars = [];
		} in
		let findVar (v:string) = begin try
			List.find (fun (x:var) -> x.id = v) state.vars
		with
			| Not_found -> raise(CompilationError("reference to unbound variable \x1b[1m"^v^"\x1b[0m",r.filename))
		end in
		let setDefer name offset skip =
			state.defers <- {id = name; addr = state.addr + offset; skip=skip} :: state.defers in
		let findSection (d:defer) : int =
			let rec loop (x:section list) = match x with
				| {id} as res::rs ->
					(if id=d.id then 
						(if d.skip then res.stop else res.start)
					else loop rs)
				| [] -> raise (CompilationError ("no section named "^d.id, r.filename))
			in loop state.sections in
		let i16 (p:int) : int list = [(p lsr 8) land 0xff; p land 0xff] in
		let i32 (p:int) : int list = [(p lsr 24) land 0xff; (p lsr 16) land 0xff; (p lsr 8) land 0xff; p land 0xff] in
		let rec asm (inp:node list) : unit =
			let addCode (c:int list) : unit =
				state.code <- state.code @ c;
				state.addr <- state.addr + List.length c in
			match inp with
				| Box((DAtom "sec"), Some (DAtom lbl), ast)::rs ->
					let start = state.addr in asm ast;
					state.sections <- {id = lbl; start = start; stop=state.addr} :: state.sections;
					asm rs
				| Directive("go" as kind, Some(DAtom lbl))::rs
				| Directive("skip" as kind, Some(DAtom lbl))::rs ->
					setDefer lbl 1 (kind="skip");
					addCode [0x12; 0x00; 0x00; 0x0e]; (* PTR 0; JMP *)
					asm rs
				| Box((DAtom "say"), None, vals)::rs ->
					let rec pushVals (v:node list) (tys : ltype list) : (int list * ltype list) = match v with
						| (Value(DStr x))::rs -> let nodes, tys = pushVals rs (LStr :: tys) in
							((0x13::bitstr_of_string x) @ nodes, tys)
						| (Value(DInt x))::rs -> let nodes, tys = pushVals rs (LInt :: tys) in (* very silly, but it's a test *)
							(0x01 :: (i32 x) @ nodes, tys)
						| (Directive(x, None))::rs -> let src = findVar x in
							let nodes, tys = pushVals rs (src.ty :: tys) in
							(0x12 :: (i16 src.pos) @ [0x18] @ nodes, tys)
						| [] -> [], tys
						| _ -> raise(CompilationError("unsupported value in say box",r.filename))
					in let ops, tys = pushVals vals [] in
						addCode (ops @ [0x13] @ (map int_of_type (List.rev tys)) @ [0;0x14]);
					asm rs
				| Definition(name, DFlag(v))::rs ->
					state.vars <- {id=name; ty=LFlag; pos=state.stackp} :: state.vars;
					state.stackp <- state.stackp + 1;
					addCode [if v = true then 0x20 else 0x21];
					asm rs
				| Definition(name, DInt(v))::rs ->
					state.vars <- {id=name; ty=LInt; pos=state.stackp} :: state.vars;
					state.stackp <- state.stackp + 1;
					addCode (0x01 :: (i32 v));
					asm rs
				| Definition(name, DStr(v))::rs ->
					state.vars <- {id=name; ty=LStr; pos=state.stackp} :: state.vars;
					state.stackp <- state.stackp + 1;
					addCode (0x13 :: bitstr_of_string(v) @ [0x18]);
					asm rs
				| Definition(name, DAtom(v))::rs ->
					let src = (findVar v) in
					state.vars <- {id=name; ty=src.ty; pos=state.stackp} :: state.vars;
					state.stackp <- state.stackp + 1;
					addCode (0x12 :: i16(src.pos) @ [0x18]);
					asm rs
				| Assignment(name, v)::rs ->
						let var = findVar name in
						(match (v, var.ty) with
							| DFlag true,  LFlag -> addCode (0x20::0x12::(i16 var.pos)@[0x17])
							| DFlag false, LFlag -> addCode (0x21::0x12::(i16 var.pos)@[0x17])
							| DAtom vl, ty       -> let src = findVar vl in
								if src.ty <> ty then raise(CompilationError("variables "^name^" and "^vl^" are of different types",r.filename))
							| DInt i, LInt       -> addCode (0x20::0x01::(i32 var.pos)@[0x17])
							| DStr s, LStr -> raise(CompilationError("attempted to assign to string constant \x1b[1m"^name^"\x1b[0m", r.filename))
							| _, ty -> raise(CompilationError("type mismatch: \x1b[1m"^name^"\x1b[21m is of type \x1b[1m"^(str_of_type ty)^"\x1b[21m", r.filename)));
						asm rs
				| Directive("msg", Some(DStr txt))::rs ->
					state.code <- state.code @ ((0x13 :: bitstr_of_string txt) @ [0x14]); (* STR txt; MSG *)
					state.addr <- state.addr + 3 + String.length txt;
					asm rs
				| Box(DAtom("choice"), None, nds)::rs ->
					let findBehavior (s:string) : int =
						let rec loop (i:int) (b:behavior) : int = match b with
						| b::rs -> if b.id = s then i else loop i+1 rs
						| _ -> expr2
							
					let readChoice (x:node) : choice = match x with
						| Box(DAtom(id), Some(DStr(txt)), cont) -> {
							bhv = findBehavior id;
							name = txt;
							nodes = cont;
						}
						| Box(DStr(txt), None, cont) -> {
							bhv = 0;
							name = txt;
							nodes = cont;
						}
						| _ -> raise(CompilationError("invalid construct in choice block", r.filename))
					in
					let choices = map readChoice nds in
						addCode (map (fun x -> x.bhv :: (bitstr_of_string x.name)) choices);
						let start_of_conditionals = state.addr in
						times (List.length choices) (fun i ->
							addCode ([0x12] @ (i16 i) @ [0x31, 0x12, 0x00,0x00, 0x08]));
						let entries : int list cell = { [] };
						each choices (fun x -> entries.v <- state.addr; (print_string (x.name^"\n")); asm x.nodes )
						each entries (fun x -> Printf.printf "entry: %d" x)
						(* PTR idx CND PTR deferred-br JE *)
						
					asm rs
				| [] -> ()
				| _ -> raise (CompilationError("wat",r.filename))
		in dump_ast r.dat; asm r.dat;
		state.code <- state.code @ [0]; (* ensure hlt *)
		let cstr : bytes = bitstr state.code in
		let rec processDefers (deflist: defer list) = match deflist with
			| x::rs ->
				let addr = findSection x in
				let b1 = (addr lsr 8) land 0xff in
				let b2 = addr land 0xff in
				Bytes.set cstr x.addr (char_of_int b1);
				Bytes.set cstr (x.addr+1) (char_of_int b2);
				processDefers rs
			| [] -> ()
		in processDefers state.defers;
		cstr;
end;;

type game = {
	name : string;
	start: string * int * int;
	vars : string list;
	flags: string list;
	characters: character list;
}


exception CollectError
let rec collect (l:node list) : literal list =
	match l with
		| Value(a)::rs -> a::collect(rs)
		| [] -> []
		| _ -> assert false

let addSources (ct:manifest_data) (kind:string) (files:string list) : manifest_data =
	match kind with
	| "char"   -> {ct with src_char    = files}
	| "script" -> {ct with src_script = files}
	| _ -> raise ( CompilationError("unknown source type!", "manifest") );;

exception PropMissing of string
let rec getProp (x:node list) (prop:string) : literal = match x with
	| Directive(nm, Some v)::rs -> if nm = prop then v else (getProp rs prop)
	| _::rs -> getProp rs prop
	| [] -> raise (PropMissing prop);;

let processManifest (filename:string) : manifest_data =
	let ast = qparse(filename) in
	let mfd : manifest_data = { (* game defaults *)
		title = "Untitled Game";
		src_char = [];
		src_script = [];
		bhv = [];
	} in
	let rec processBehaviors (x:node) : behavior = 
		let ty_mism () = raise(CompilationError("type mismatch in behavior field!",filename)) in
		match x with Box((DAtom bid), None, nds) -> begin try {
			id = bid;
			img = (match getProp nds "img" with DStr(x) -> x | _ -> ty_mism());
			name = match getProp nds "name" with DStr(x) -> x | _ -> ty_mism();
		} with PropMissing x ->
			raise(CompilationError("missing property \x1b[1m"^x^"\x1b[21m in behavior entry for \x1b[1m"^bid^"\x1b[21m",filename))
		end
		| _ -> raise(CompilationError("invalid construct in behavior section!", filename))
	in
	let rec loop (ct:manifest_data) (s:node list) = match s with
		| Directive("title", Some(DStr t))::rs -> loop {ct with title=t} rs
		| Box((DAtom "src"), Some(DAtom kind), nodes)::rs -> 
			let files:string list = map (fun (x:node) -> match x with
				| Value(DStr(file)) -> file
				| _ -> raise (CompilationError("source files must be of type string!",filename))
			) nodes in loop (addSources ct kind files) rs
		| Box((DAtom "behavior"), None, nodes)::rs ->
			{ct with bhv = ct.bhv @ map processBehaviors nodes}
		| [] -> ct
		| _ -> raise (CompilationError ("invalid construct!", filename))
	in loop mfd ast;;

let dump_manifest (m:manifest_data) : unit = 
	let rec dump_list nm l : unit = match l with
	| h::rs -> print_string("\x1b[1m"^nm^"\x1b[0m: \""^h^"\"\n"); dump_list nm rs
	| [] -> ()
	in
	each m.bhv (fun x -> Printf.printf "%s (%s)" x.name x.id);
	Printf.printf "title: %s\n" m.title;
	dump_list "Character source" m.src_char; dump_list "Script source" m.src_script;;

let processChar (fn:string) (cid:string) (ast:node list) : character =
	let base : character = {
		name="";
		id=cid;
		face="";
		spriteset="";
	} in
	let rec scan (ns:node list) (c:character) : character = match ns with
		| Directive("name", Some(DStr str))::rs -> scan rs {c with name=str}
		| Directive("face", Some(DStr str))::rs -> scan rs {c with face=str}
		| Directive("spriteset", Some(DStr str))::rs -> scan rs {c with spriteset=str}
		| [] -> c
		| _ -> raise (CompilationError("unrecognized directive!", fn))
	in scan ast base;;

let dumpChar (c:character) : unit =
	print_string (c.id^": \n - name: "^c.name^"\n - face: "^c.face^"\n");;

let processCharFile (filename:string) : character list =
	let ast = qparse(filename) in
	let rec loop (i:node list) : character list = match i with
		| Box(DAtom "char",Some(DAtom id),dat)::rs ->
			(processChar filename id dat)::loop rs
		| [] -> []
		| _ -> raise (CompilationError ("invalid char entry", filename))
	in loop ast;;

let rec fold (fn: 'a -> 'b list) (x:'a list) : ('b list) = match x with
	| h::rs -> (fn h) @ (fold fn rs)
	| [] -> [];;

let file_err t fn (e: string option) = err("\x1b[1m"^t^"\x1b[0m in \x1b[1m"^fn^
	match e with
	| Some s -> ": \x1b[0;31m"^s
	| None -> "")



let () = if Array.length Sys.argv != 2 then
	err ("\x1b[1musage:\x1b[0m "^Sys.argv.(0)^" project folder")
else try (
	dir_base.v <- Sys.argv.(1);
	let manifest = processManifest      "manifest"          in
	let chars    = fold processCharFile manifest.src_char   in
	let scripts  = fold Scripting.processFile   manifest.src_script in
	let scripts_text = map (fun x -> Scripting.compile x scripts manifest) scripts in
	each scripts_text print_string;
	dump_manifest manifest; each chars dumpChar
) with
	| CompilationError(x,filename) -> file_err "compilation error" filename (Some x)
	| ParseError(filename) -> file_err "parsing failed" filename None;