#pragma once
namespace L {
	namespace err {
		struct err {
			void dump();
			virtual const char* message() = 0;
		};
	}
}
