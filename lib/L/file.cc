#include "io.h"
#include "file.h"
#include "pfile.h"
#include "str.h"
namespace L {
	namespace io {
		file::file(const char* name, i8 mode) { fd = open(name,mode); }
		file::~file() {close(fd);}
		char file::getc() {
			char out = 0;
			io::get(fd,&out,1);
			return out;
		}
		void file::get(char* out, size_t ct)		{ io::get(fd, out, ct); }
		void file::put(const char* in, size_t ct)	{ io::put(fd, in, ct); }
		void file::put(const char* in)				{ io::put(fd, in); }
		size_t file::size() {
			size_t r = io::end(fd);
			io::start(fd);
			return r;
		}
		char* file::load() {
			size_t sz = size();
			char* c = new char[sz+1];
			get(c,sz);
			c[sz]=0;
			return c;
		}
	}
}
