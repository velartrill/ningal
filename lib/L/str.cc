#include "str.h"
#include "io.h"
namespace L {
	namespace str {
		size_t len(const char* i) {
			size_t ct = 0;
			while (*i++!=0 and ++ct);
			return ct;
		}
		bool eq(const char* s1, const char* s2) {
			while(*s1==*s2)
				if (*s1==0) return true;
				else ++s1, ++s2;
			return false;
		}
		void cat(char* dest, const char* src) {
			while (*dest!=0) ++dest;
			while ((*dest++ = *src++));
		}
		char* dup(const char* b, size_t l) {
			char* str = new char[l],* r = str;
			while (l-->0) {*r++ = *b++;}
			//break
			return str;
		}
		char* dup(const char* b) {
			return dup(b,len(b));
		}
		void compose(char* dest, const char* cp) {
			while ((*dest++ = *cp++));
		}
	}
}
