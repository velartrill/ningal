#pragma once
#include <cassert>
namespace L {
template <typename T> struct stack {
	struct node {
		T val;
		node* last;
	};
	node* top;
	stack() {
		top = nullptr;
	}
	T& operator++() {
		node* newnode = new node;
		newnode->last = top;
		top = newnode;
		return newnode->val;
	}
	T operator--() {
		T v = top->val;
		node* t = top;
		top = top->last;
		delete t;
		return v;
	}
	T& operator*() {
		return top->val;
	}
	void _delnode(node* n) {
		node* next = n->last;
		delete n;
		if (next!=nullptr) _delnode(next);
	}
	~stack() {
		if (top!=nullptr) _delnode(top);
	}
	bool empty() {return top==nullptr;}
};
}
