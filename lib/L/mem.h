#pragma once
#include "def.h"

namespace L {
	void* resize (void* ptr, L::size_t sz);
}
