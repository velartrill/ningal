#include "info.h"
#include "io.h"
#include "str.h"
#include "mem.h"
#if !(defined __x86_64__ && defined __linux__)
	namespace libc { extern "C" {
	#include <unistd.h>
	}}
#endif
namespace L {
	namespace io {
		void put(char c) {
			put(out, &c, 1);
		}
		void put(fildes f, char c) {
			put(f,&c,1);
		}
		void put(const char* s) {
			put(out,s,str::len(s));
		}
		void put(fildes f, const char* s) {
			put(f,s,str::len(s));
		}
		void put(const char* s, size_t i) {
			put(out, s,i);
		}
		void put(fildes f, const char* s, size_t i) {
			#if defined __x86_64__ && defined __linux__
			asm volatile (
				"mov $1, %%rax;"
				"mov %[f], %%rdi;"
				"mov %[s], %%rsi;"
				"mov %[i], %%rdx;"
				"syscall;"
				
				:: [f]"m"(f),
				   [s]"m"(s),
				   [i]"m"(i)
			);
			#else
				libc::write(f,s,i);
			#endif
		}
		size_t get(char* c, size_t ct) {
			return get(in,c,ct);
		}
		size_t get(fildes fd, char* c, size_t ct) {
			#if defined __x86_64__ && defined __linux__
			size_t ret;
			asm volatile (
				"mov $0, %%rax;"
				"mov %[fd], %%rdi;"
				"mov %[c], %%rsi;"
				"mov %[ct], %%rdx;"
				"syscall;"
				"mov %%rax, %[ret]"

				: [ret]"=g"(ret)
				: [fd] "m" (fd),
				  [c]  "m" (c),
				  [ct] "m" (ct)
			);
			return ret;
			#else
			#	error "get() is not defined for this platform"
			#endif
		}
		char* input(size_t* lenv) {
			constexpr i8 bufsize = 32;
			size_t run = 0, len;
			char* buf = new char[bufsize];
			for (;;) {
				size_t ct = get(in,buf+(bufsize*(run)),bufsize);
				if (ct == bufsize) {
					++run;
					buf = (char*)resize(buf, sizeof(char)*bufsize*run);
				} else {
					len = bufsize*run+ct-1;
					buf[len] = 0;
					break;
				}
			};
			if (len<bufsize*run) buf = (char*)resize(buf, sizeof(char)*len);
				// compress
			if (lenv != nullptr) *lenv = len;
			return buf;

		}
	}
}
