#pragma once
#include "info.h"

namespace L {
	template <typename T> struct rng {
		T x, y, z, w;
	
		void seed(T s) { // this is a kinda silly impl but w/e
			x = s;
			y = x << sizeof(T) / 3;
			z = z ^ y;
			w = z << (sizeof(T) / 2) ^ x;
		}
		T raw() {
			T t = x ^ (x << 11);
			x = y; y = z; z = w;
			return w = w ^ (w >> 19) ^ t ^ (t >> 8);
		}
		T range(T min, T max) {
			return raw() % ((max+1)-min) + min;
		}
		template<typename A, size_t N> const A& pick(const A (&t) [N]) {
			return t[range(0,N-1)];
		}
	};
}
