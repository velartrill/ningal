/********************************************************************************\
| PFILE.H                                                                        |
|--------------------------------------------------------------------------------|
| This header contains the definition for the procedural file control interface. |
| It acts as a wrapper around kernel syscalls where possible, and falls back on  |
| libc where not.                                                                |
\********************************************************************************/

#pragma once
#include "def.h"
#include "file-err.h"
namespace L {
	namespace io {
		constexpr i8
			r = 1, rw = 1<<1, w = 1<<2,
			append = 1<<3,
			truncate = 1<<4;
		fildes open(const char* name, i8 mode = r);
		void close(fildes);
		size_t start(fildes);
		size_t end(fildes);
	}
}
