#pragma once
#include "def.h"
#include "mem.h"
#include "err.h"
#include "ptr.h"
namespace L {
	namespace str {
		// 128-bit ints are 39 digits long at max. if this library
		// is ever ported to 256-bit hardware you'll have to revise
		// this constant upwards. otherwise, 256-bit ints will be
		// truncated.
		constexpr size_t max_digits = 39;
		
		/* str::len
		 * ---------
		 * returns the length of a string _str_ in bytes.
		 
		 = allocations: 0     = frees: 0
		 = class: O(n), where str has n bytes
		 = stack req: nil     = threadsafe
		 */
			size_t len(const char* str);
		void cat(char* dest, const char* src);
		bool eq(const char* s1, const char* s2);
		/* str::dup(const char* string[, size_t length])
		 * ---------
		 * allocates a copy of _str_. copies _len_ bytes, otherwise copies until null
		 * found.
		 
		 = allocations: 1     = frees: 0
		 = class: O(len)      = threadsafe
		 = stack req: sizeof(char*) bytes
		 */
			char* dup(const char* str, size_t len);
			char* dup(const char* str);
		
		/* string type
		 * -----------
		 * L::str::string is a boxed string convenience type. it is automatically freed
		 * when leaving scope. see L::ptr::box for precise semantics. values of type
		 * L::str::string should be returnd by all functions returning temporary strings.
		 */
			typedef ptr::box<char> string;
		
		/* str::from
		 * ---------
		 * str::from takes an integer _n_ of any type _T_ and returns a boxed string
		 * (L::str::string, aka L::ptr::box<char>) representing that string in the
		 * given base _base_.
		 
		 = allocations: 1     = frees: 0
		 = class: O(log n)    = threadsafe
		 = stack req: 3 + (max_digits+1) + sizeof(char*) bytes 
		 */
			template <typename T> string from(T n, size_t* len = nullptr, i8 base = 10) {
				char b[max_digits+1], *p=&b[max_digits-1];
				b[max_digits]=0;
				size_t sz = 1;
				do {
					i8 v = n % base;
					char a;
					if (v<10) a='0'+v;
					else a='a'+(v-10);
					*p --= a;
					n /= base;
					++sz;
				} while (n != 0 and p>=b);
				if (len!=nullptr) *len=sz-1;
				return dup(p+1, sz);
			}
		/* str::buffered_from
		 * ---------
		 * a faster version of from that makes no calls to malloc and operates on a
		 * shared buffer. buffered_from should be used wherever performance is
		 * important. an L::str::charbuf must be declared and passed to the function.
		 
		 = allocations: 0     = frees: 0
		 = class: O(log n)    = threadsafe
		 = stack req: 3 + sizeof(char*) bytes
		 */
			using charbuf = char[max_digits+1];
			template <typename T> char* buffered_from (charbuf buf, T n, size_t* len = nullptr, i8 base = 10) { 
				char *p=&buf[max_digits-1];
				buf[max_digits]=0;
				size_t sz = 1;
				do {
					i8 v = n % base;
					char a;
					if (v<10) a='0'+v;
					else a='a'+(v-10);
					*p --= a;
					n /= base;
					++sz;
				} while (n != 0 and p>=buf);
				if (len!=nullptr) *len=sz-1;
				return p+1;
			}
		
		/* str::compose
		 * ---------
		 * str::compose takes a pack of char*s and composes them into the string
		 * dest.
		 : compose(str, "this", " ", "is ", "a test");
		   -> str contains "this is a test"
		 = allocations: 0     = frees: 0
		 = class: O(n)        = threadsafe
		 */
		 	void compose(char* dest, const char* cp);
			template <typename... T> void compose(char* dest, const char* cp, T... args) {
				while ((*dest++ = *cp++));
				compose(dest-1, args...);
			}
			
		/* end of string library */
	}
}
