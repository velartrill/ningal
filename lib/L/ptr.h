#pragma once
#include "mem.h"
namespace L {
	namespace ptr {
		/* L::ptr::box
		   -----------
		   contains an owned pointer. once the value of a box is invalidated, either by
		   leaving scope or replaced with another pointer, the contents are freed.
		   boxes may be dereferenced by the * operator.
		*/
		template <typename T> class box {
			private:
				T* _ptr;
			public:
				box(T* ptr) {_ptr=ptr;}
				T& operator*() {return *_ptr;};
				operator T*() {return _ptr;};
				void operator=(box<T> t) {
					delete _ptr;
					_ptr = t._ptr;
					t._ptr = nullptr;
				}
				T* operator=(T* ptr) {
					delete _ptr;
					_ptr = ptr;
				}
				~box() {delete _ptr;}
		};
		/* L::ptr::ref
		   -----------
		   the ref pointer implements basic reference-counted garbage collection. it may
		   be dereferenced with the * operator. all refs refering to the same object
		   maintain a link to a heap-allocated counter.
		   
		   allocation:	ref<obj> i = new obj(param1, param2);
		   
		   caveats: * ref<T> operator=(T*) syntax is provided only for use with new and
		              must never be used to assign an existing pointer to a ref instance.
		              in such a case, ref would not be able to accurately determine the
		              pointer's number of users and might free the object while it is
		              still needed.
		            * ref<T> does not account for cyclical dependencies.
		            
		   GENERAL WARNING: garbage collection should be avoided whenever possible.
		                    ref<T> is provided for contingencies where GC is genuinely
		                    required, not as a replacement for manual or region-based
		                    memory-management à la L::ptr::box.
		*/
		template <typename T> class ref {
			private:
				T* _ptr;
				size_t* _ctr;
				void dec() {
					if (_ctr==nullptr) return;
					--(*_ctr);
					if (*_ctr == 0) {
						delete _ctr;
						delete _ptr;
					}
				}
			public:
				ref(T* ptr) {
					_ptr = ptr;
					*(_ctr = new size_t) = 1;
					
				}
				ref(decltype(nullptr)) {
					_ptr = nullptr;
					_ctr = nullptr;
				}
				ref(const ref<T>& ptr) {
					operator=(ptr);
				}
				~ref() {
					dec();
				}
				ref<T>& operator=(const ref<T>& ptr) {
					_ptr = ptr._ptr;
					_ctr = ptr._ctr;
					++(*_ctr);
					return *this;
				}
				T* operator=(T* ptr) {
					dec();
					_ptr = ptr;
					*(_ctr = new size_t) = 0;
				}
				T* operator=(decltype(nullptr)) {
					dec();
					_ptr = nullptr;
				}
				bool operator==(decltype(nullptr)) const { return _ptr == nullptr; }
				bool operator==(const ref<T>& t) const { return t._ptr == _ptr; }
				template <typename X> explicit operator ref<X>() const {return ref<X>{(X*)_ptr,_ctr};}
				T& operator*() const { return *_ptr; }
		};
	}
}
