#include "pfile.h"
#include <fcntl.h>
#include <unistd.h>
#include "io.h"
namespace L {
	namespace io {
		fildes open(const char* name, i8 flags) {
			int ofl=0, ofm=0;
			if (flags & r) ofl = O_RDONLY;
			if (flags & rw) ofl = O_RDWR;
			if (flags & w) ofl = O_WRONLY;

			if (flags & truncate) ofl |= O_TRUNC;
			if (flags & append) ofl |= O_APPEND;
			fildes f;
			#ifdef __x86_64__
			// make the syscall here by hand so we don't incur
			// function call overhead.
				asm volatile (
					"movq $2, %%rax;"
					"mov %[name], %%rdi;"
					"movq %[ofl], %%rsi;"
					"movq $0, %%rdx;"
					"syscall;"
					//"movb %%rax, %0;"

					: "=g"(f)
					: [name]"m"(name),
					  [ofl] "m"(ofl)
				);
			#else
				f = open(name,ofl);
			#endif
			/*if (f<0)
				throw err::open();*/
			return f;
		}
		void close(fildes fd) {
			#ifdef __x86_64__
			// make the syscall here by hand so we don't incur
			// function call overhead.
				asm volatile (
					"movq $3, %%rax;"
					"movq %[fd], %%rdi;"
					"syscall;"
					:: [fd]"m"(fd)
				);
			// TODO: impl syscalls for other arches
			#else
				close(fd);
			#endif
		}
		size_t start(fildes fd) {
			#ifdef __x86_64__
				size_t r;
				asm volatile (
					"movq $8, %%rax;" "movq %[fd], %%rdi;"
					"movq $0, %%rsi;" "movq $0, %%rdx;"
					"syscall;" "movq %%rax, %[r];"
					: [r]"=g"(r)
					: [fd]"m"(fd)
				);
				return r;
			// TODO: impl syscalls for other arches
			#else
				return lseek(fd,0,SEEK_SET);
			#endif
		}
		size_t end(fildes fd) {
			#ifdef __x86_64__
				size_t r;
				asm volatile (
					"movq $8, %%rax;" "movq %[fd], %%rdi;"
					"movq $0, %%rsi;" "movq $2, %%rdx;"
					"syscall;" "movq %%rax, %[r];"
					: [r]"=g"(r)
					: [fd]"m"(fd)
				);
				return r;
			// TODO: impl syscalls for other arches
			#else
				return lseek(fd,0,SEEK_END);
			#endif
		}
	}
}
