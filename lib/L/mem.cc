#include <stdlib.h>
void* operator new (size_t sz) {
	return malloc(sz);
}
void* operator new[] (size_t sz) {
	return malloc(sz);
}
void operator delete (void* ptr) noexcept {
	free(ptr);
}
void operator delete[] (void* ptr) noexcept {
	free(ptr);
}
namespace L {
	void* resize (void* ptr, size_t sz) {
		return realloc(ptr,sz);
	}
}
