namespace L {
	namespace math {
		template <typename T> T abs(T v) { return (v < 0 ? -v : v); }
		template <typename T> T max(T v1, T v2) { return (v1>v2?v1:v2); }
		template <typename T> T min(T v1, T v2) { return (v1<v2?v1:v2); }
	}
}