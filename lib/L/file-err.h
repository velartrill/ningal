/********************************************************************************\
| PFILE-ERR.H                                                         [INTERNAL] |
|--------------------------------------------------------------------------------|
| This is a header that defines file exceptions common to the procedural and OO  |
| interfaces. It is automatically included from both and should not be included  |
| from user code in ordinary circumstances.                                      |
\********************************************************************************/

#pragma once
#include "err.h"
namespace L {
	namespace err {
		struct open : public err {
			const char* message() { return "cannot open file"; }
		};
	}
}