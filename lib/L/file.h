/********************************************************************************\
| FILE.H                                                                         |
|--------------------------------------------------------------------------------|
| This header contains the definition for the object-oriented file control       |
| interface. It uses functions defined in pfile.h and io.h to implement this     |
| interface.                                                                     |
\********************************************************************************/

#pragma once
#include "def.h"
#include "file-err.h"
namespace L {
	namespace io {
		struct file {
			fildes fd;
			file(const char* name, i8 mode = 1);
			~file();
			char getc();
			void get(char* out, size_t ct);
			void put(const char* in, size_t ct);
			void put(const char* in);
			char* load();
			size_t size();
		};
	}
}
