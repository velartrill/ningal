#pragma once
#include "io.h"
#include "str.h"
namespace L {
	template <typename T> void $$(T v) { // ints
		size_t len;
		char* c = str::from(v, &len);
		io::put(c,len);
		io::put(' ');
		delete[] c;
	}
	template <typename T> struct base {
		T val;
		i8 base;
	};
	template <typename T> void $$(base<T> v) {
		size_t len;
		char* c = str::from(v.val, &len, v.base);
		io::put(c,len);
		io::put(' ');
		delete[] c;
	}
	template <> void $$(const char* v) {
		io::put("\e[1m");
		io::put(v);
		io::put(":\e[0m ");
	}
	template <typename T> void $(T t) {
		$$(t);
	}
	template <typename T, typename... X> void $(T t, X... Argv) {
		$$(t);
		$(Argv...);
	}
}
