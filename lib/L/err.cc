#include "err.h"
#include "io.h"
namespace L {
	namespace err {
		void err::dump() {
			io::put(io::err, " \e[31;1;7m ERR \e[0;1m ");
			io::put(io::err, message());
			io::put(io::err, "\e[0m\n");
		}
	}
}
