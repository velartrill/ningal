#pragma once
#define val constexpr static
namespace L {
/* integer types */
	namespace types { // allow types to be imported separately...
		typedef decltype(sizeof(int)) size_t; // i regret nothing
		namespace {
			#define entry(i,_u,_s) template <> struct entry<i>{using u=_u;using s = _s;}
			template <unsigned char index> struct entry { using s = void; using u = void; };
			entry(0, unsigned char,       signed char);
			entry(1, unsigned short,      signed short);
			entry(2, unsigned int,        signed int);
			entry(3, unsigned long,       signed long);
			entry(4, unsigned long long,  signed long long);
			entry(5, __uint128_t, __int128_t);
			#undef entry
	
			template <size_t bits, size_t i> struct select {
				val size_t v = (
					sizeof(typename entry<i>::u)*8 == bits
						? i
						: select<bits,i+1>::v
				);
			};
			
			template <size_t bits> struct select<bits, 6> { val size_t v = 6; };
		}

		template <size_t bits> struct ints {
			using u = typename entry<select<bits,0>::v>::u;
			using s = typename entry<select<bits,0>::v>::s;
		};
		
		typedef ints<8>   :: u    i8;
		typedef ints<16>  :: u    i16;
		typedef ints<32>  :: u    i32;
		typedef ints<64>  :: u	   i64;
		typedef ints<128> :: u    i128; // why not
		
		typedef ints<8>   :: s    s8;
		typedef ints<16>  :: s    s16;
		typedef ints<32>  :: s    s32;
		typedef ints<64>  :: s	   s64;
		typedef ints<128> :: s    s128; // again, why not
	}
	using namespace types; // ...but keep them in the main namespace for convenience
	
	template <typename T> struct limits {
		// assuming 2's-comp arch
		val bool is_signed = ((T)~0) < 0;
		val T min = (is_signed? (T)1<<(sizeof(T)*8-1) : 0);
		val T max = ~min;
	};

	template <typename T, size_t N> constexpr size_t sz(const T (&t) [N]) {
		return N;
	}
/* misc */

	typedef decltype(nullptr) null_t;
	typedef int fildes;	

}
#undef val
