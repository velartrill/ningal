#pragma once
#include "def.h"
namespace L {
	namespace io {
		constexpr int in = 0, out = 1, err = 2;
		/* friendlier wrappers for write(3)/fputc(3) */
		void put(char c);
		void put(fildes f, char c);
		void put(const char* str);
		void put(fildes f, const char* str);
		void put(const char* str, size_t i);
		void put(fildes f, const char* str, size_t i);

		size_t get(char* c, size_t ct);
		size_t get(fildes fd, char* c, size_t ct);

		char* input(size_t* lenv = nullptr);
	}
}
